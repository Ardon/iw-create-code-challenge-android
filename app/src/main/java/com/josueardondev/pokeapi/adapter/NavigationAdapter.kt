package com.josueardondev.pokeapi.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.josueardondev.pokeapi.view.FavoritesFragment
import com.josueardondev.pokeapi.view.PokemonFragment


class NavigationAdapter(
    fm: FragmentManager
): FragmentStatePagerAdapter(fm) {

    private val mFragments = mutableListOf<Fragment>().apply {
        add(PokemonFragment())
        add(FavoritesFragment())
    }

    override fun getItem(position: Int): Fragment {
        return mFragments[position]

    }

    override fun getCount(): Int {
        return mFragments.size

    }

}