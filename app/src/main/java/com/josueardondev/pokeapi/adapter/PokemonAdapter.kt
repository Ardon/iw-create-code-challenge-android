package com.josueardondev.pokeapi.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.josueardondev.pokeapi.R
import com.josueardondev.pokeapi.api.model.pokemon.Pokemon
import com.josueardondev.pokeapi.database.entitiy.pokemon.PokemonEntity
import com.josueardondev.pokeapi.util.Constants
import com.josueardondev.pokeapi.util.setGlideImage
import com.josueardondev.pokeapi.view.DetailActivity
import com.josueardondev.pokeapi.view.MainActivity
import kotlinx.android.synthetic.main.item_pokemon.view.*

class PokemonAdapter(
    private val context: Context,
    private val activity: MainActivity,
    private val pokemonList: List<Pokemon>?,
    private val localList: List<PokemonEntity>?
) : RecyclerView.Adapter<PokemonAdapter.ViewHolder>() {

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val pokemonRender: ImageView = view.pokemon_render_imageView
        val pokemonName: TextView = view.pokemon_name_textView

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon, parent, false))
    }

    override fun getItemCount(): Int {
        var size = 0
        if (pokemonList != null && localList == null) {
            size = pokemonList.size
        } else if (localList != null && pokemonList == null) {
            size = localList.size
        }

        return size

    }

    @SuppressLint("SetTextI18n", "DefaultLocale")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //If adapter is for internet data
        if (pokemonList != null && localList == null) {

            val pokemon = pokemonList[position]

            //Display data
            val number = (position + 1)
            holder.pokemonRender.setGlideImage(context, Constants.IMAGES_URL + number + ".png")
            holder.pokemonName.text = "#$number " + pokemon.name!!.capitalize()

            //Go to details
            holder.itemView.setOnClickListener {
                val intent = Intent(context, DetailActivity::class.java)
                intent.putExtra(Constants.EXTRA_LOAD_LOCAL, false)
                intent.putExtra(Constants.EXTRA_POKEMON, pokemon)
                activity.startActivityForResult(intent, 1)
            }

        }

        else if (localList != null && pokemonList == null)

        {
            val localPokemon = localList[position]
            holder.pokemonRender.setGlideImage(context, Constants.IMAGES_URL + localPokemon.id + ".png")
            holder.pokemonName.text = "#${localPokemon.id} " + localPokemon.name.capitalize()

            //Go to details locally
            holder.itemView.setOnClickListener {
                val intent = Intent(context, DetailActivity::class.java)
                intent.putExtra(Constants.EXTRA_LOAD_LOCAL, true)
                intent.putExtra(Constants.EXTRA_FAVORITE, localPokemon)
                activity.startActivityForResult(intent, 1)
            }

        }

    }

}