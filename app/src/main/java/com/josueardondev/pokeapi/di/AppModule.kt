package com.josueardondev.pokeapi.di

import android.content.Context
import androidx.room.Room
import com.josueardondev.pokeapi.api.PokeApiInterface
import com.josueardondev.pokeapi.database.PokemonDatabase
import com.josueardondev.pokeapi.util.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    // Providers //

    @Singleton
    @Provides
    fun provideRetrofit(): PokeApiInterface {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Constants.BASE_URL)
            .build()
            .create(PokeApiInterface::class.java)
    }

    @Provides
    fun provideDatabase(
        @ApplicationContext appContext: Context
    ): PokemonDatabase {
        return Room.databaseBuilder(
            appContext,
            PokemonDatabase::class.java,
            "pokemon_db"
        ).fallbackToDestructiveMigration().build()

    }

}