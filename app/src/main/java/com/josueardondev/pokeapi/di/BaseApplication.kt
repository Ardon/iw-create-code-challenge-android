package com.josueardondev.pokeapi.di

import android.app.Application
import android.content.Context
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BaseApplication: Application() {

    companion object {

        private lateinit var baseAppContext: Context

        fun getAppContext(): Context {
            return baseAppContext
        }

    }

    override fun onCreate() {
        super.onCreate()

        baseAppContext = this

    }

}