package com.josueardondev.pokeapi.util

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.net.ConnectivityManager
import android.net.Uri
import android.provider.MediaStore
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.josueardondev.pokeapi.di.BaseApplication

fun toast(message: String) {
    Toast.makeText(BaseApplication.getAppContext(), message, Toast.LENGTH_SHORT).show()
}

fun ImageView.setGlideImage(context: Context, url: String) {
    val options = RequestOptions()
        .override(200, 200)
        .centerCrop()

    Glide.with(context)
        .load(Uri.parse(url))
        .apply(options)
        .into(this)
}

fun String.switchScript(): String {
    return this.replace("-", " ")
}

fun Int.toDp(): Int {
    return (this * Resources.getSystem().displayMetrics.density).toInt()
}

fun isNetworkAvailable(): Boolean {
    val connectivityManager =
        BaseApplication.getAppContext()
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
    val activeNetworkInfo = connectivityManager!!.activeNetworkInfo
    return activeNetworkInfo != null && activeNetworkInfo.isConnected
}

fun View.setGone() {
    this.visibility = View.GONE
}

fun View.setVisible() {
    this.visibility = View.VISIBLE
}

fun View.setInvisible() {
    this.visibility = View.INVISIBLE
}

fun showDialog(context: Context, message: String): Dialog {
    return MaterialAlertDialogBuilder(context)
        .setMessage(message)
        .setCancelable(false)
        .create()
}

fun requestPermission(activity: Activity, permissions: Array<String>, requestCode: Int) {
    ActivityCompat.requestPermissions(activity, permissions, requestCode)
}

fun checkPermission(activity: Activity, permission: String): Boolean {
    return ContextCompat.checkSelfPermission(
        activity,
        permission
    ) == PackageManager.PERMISSION_GRANTED
}

fun shareScreen(view: NestedScrollView, context: Context) {
    val bitmap = Bitmap.createBitmap(view.width, view.getChildAt(0).height, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    view.draw(canvas)
    val path = MediaStore.Images.Media.insertImage(BaseApplication.getAppContext().contentResolver, bitmap, "Pokemon", null)
    val uri = Uri.parse(path)
    val intent = Intent(Intent.ACTION_SEND)
    intent.type = "image/png"
    intent.putExtra(Intent.EXTRA_STREAM, uri)
    context.startActivity(Intent.createChooser(intent, "Share"))
}
