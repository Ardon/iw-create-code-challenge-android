package com.josueardondev.pokeapi.util

object Constants {

    const val BASE_URL = "https://pokeapi.co/api/v2/"
    const val IMAGES_URL = "https://pokeres.bastionbot.org/images/pokemon/"

    const val EXTRA_LOAD_LOCAL = "LOAD_LOCAL"
    const val EXTRA_POKEMON = "POKEMON"
    const val EXTRA_FAVORITE = "FAVORITE"

}