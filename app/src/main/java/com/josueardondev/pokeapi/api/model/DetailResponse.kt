package com.josueardondev.pokeapi.api.model

import com.google.gson.annotations.SerializedName
import com.josueardondev.pokeapi.api.model.ability.AbilityResponse
import com.josueardondev.pokeapi.api.model.move.MoveResponse
import com.josueardondev.pokeapi.api.model.stat.StatResponse
import com.josueardondev.pokeapi.api.model.type.TypeResponse

data class DetailResponse(

    @SerializedName("id")
    var id: Int? = 0,

    @SerializedName("base_experience")
    var baseExperience: Int? = 0,

    @SerializedName("height")
    var height: Int? = 0,

    @SerializedName("name")
    var name: String? = "",

    @SerializedName("weight")
    var weight: Int? = 0,

    @SerializedName("abilities")
    var abilities: List<AbilityResponse> = arrayListOf(),

    @SerializedName("moves")
    var moves: List<MoveResponse> = arrayListOf(),

    @SerializedName("stats")
    var stats: List<StatResponse> = arrayListOf(),

    @SerializedName("types")
    var types: List<TypeResponse> = arrayListOf()

)