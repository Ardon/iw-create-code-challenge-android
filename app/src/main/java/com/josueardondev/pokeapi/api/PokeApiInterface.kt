package com.josueardondev.pokeapi.api

import com.josueardondev.pokeapi.api.model.DetailResponse
import com.josueardondev.pokeapi.api.model.pokemon.PokemonResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface PokeApiInterface {

    @GET("pokemon")
    fun pokemonList(): Call<PokemonResponse>

    @GET("pokemon/{name}")
    fun pokemonDetails(@Path("name") name: String): Call<DetailResponse>

}