package com.josueardondev.pokeapi.api.model.pokemon

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Pokemon(

    @SerializedName("name")
    var name: String? = "",

    @SerializedName("url")
    var url: String

) : Serializable