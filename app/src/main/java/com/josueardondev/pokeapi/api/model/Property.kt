package com.josueardondev.pokeapi.api.model

import com.google.gson.annotations.SerializedName

data class Property(

    @SerializedName("name")
    var name: String? = ""

)