package com.josueardondev.pokeapi.api.model.stat

import com.google.gson.annotations.SerializedName
import com.josueardondev.pokeapi.api.model.Property

data class StatResponse(

    @SerializedName("base_stat")
    var baseStat: Int? = 0,

    @SerializedName("effort")
    var effort: Int? = 0,

    @SerializedName("stat")
    var stat: Property? = Property()

)