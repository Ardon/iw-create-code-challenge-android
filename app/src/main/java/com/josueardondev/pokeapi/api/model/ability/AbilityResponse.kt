package com.josueardondev.pokeapi.api.model.ability

import com.google.gson.annotations.SerializedName
import com.josueardondev.pokeapi.api.model.Property

data class AbilityResponse(

    @SerializedName("ability")
    var ability: Property? = Property()

)