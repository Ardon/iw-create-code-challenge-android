package com.josueardondev.pokeapi.api.model.move

import com.google.gson.annotations.SerializedName
import com.josueardondev.pokeapi.api.model.Property

data class MoveResponse(

    @SerializedName("move")
    var move: Property? = Property()

)