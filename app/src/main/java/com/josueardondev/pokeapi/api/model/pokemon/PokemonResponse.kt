package com.josueardondev.pokeapi.api.model.pokemon

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class PokemonResponse(

    @SerializedName("count")
    var count: Int? = 0,

    @SerializedName("results")
    var results: List<Pokemon>? = arrayListOf()

) : Serializable