package com.josueardondev.pokeapi.api.model.type

import com.google.gson.annotations.SerializedName
import com.josueardondev.pokeapi.api.model.Property

data class TypeResponse(

    @SerializedName("slot")
    var slot: Int? = 0,

    @SerializedName("type")
    var type: Property? = Property()

)