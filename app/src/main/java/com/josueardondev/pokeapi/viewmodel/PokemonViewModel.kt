package com.josueardondev.pokeapi.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.josueardondev.pokeapi.api.PokeApiInterface
import com.josueardondev.pokeapi.api.model.pokemon.PokemonResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.await
import java.io.IOException

class PokemonViewModel @ViewModelInject constructor(
    private val pokeApi: PokeApiInterface
) : ViewModel() {

    //To observe loading
    val isLoading = MutableLiveData<Boolean>()

    //Get pokemon list
    val livePokemonResponse = MutableLiveData<PokemonResponse>()
    fun getPokemonResponse() {
        //Start loading
        isLoading.postValue(true)

        GlobalScope.launch(Dispatchers.IO) {
            try {
                //Send success response
                livePokemonResponse.postValue(pokeApi.pokemonList().await())
                isLoading.postValue(false)

            } catch (e: IOException) {
                //Send null in error case
                livePokemonResponse.postValue(null)
                isLoading.postValue(false)

            }

        }

    }

}