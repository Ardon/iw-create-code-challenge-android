package com.josueardondev.pokeapi.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.josueardondev.pokeapi.database.PokemonDatabase
import com.josueardondev.pokeapi.database.entitiy.pokemon.PokemonEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class FavoritesViewModel @ViewModelInject constructor(private val database: PokemonDatabase) :
    ViewModel() {

    val liveLocalPokemonList = MutableLiveData<List<PokemonEntity>>()
    fun getLocalPokemonList() {
        GlobalScope.launch(Dispatchers.IO) {
            try {
                liveLocalPokemonList.postValue(database.pokemonDao().getAllPokemon())

            } catch (e: IOException) {
                liveLocalPokemonList.postValue(null)

            }

        }

    }

}