package com.josueardondev.pokeapi.viewmodel

import android.annotation.SuppressLint
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.josueardondev.pokeapi.R
import com.josueardondev.pokeapi.api.PokeApiInterface
import com.josueardondev.pokeapi.api.model.DetailResponse
import com.josueardondev.pokeapi.database.PokemonDatabase
import com.josueardondev.pokeapi.database.entitiy.ability.AbilityEntity
import com.josueardondev.pokeapi.database.entitiy.move.MoveEntity
import com.josueardondev.pokeapi.database.entitiy.pokemon.PokemonEntity
import com.josueardondev.pokeapi.database.entitiy.stat.StatEntity
import com.josueardondev.pokeapi.database.entitiy.type.TypeEntity
import com.josueardondev.pokeapi.di.BaseApplication
import com.josueardondev.pokeapi.util.toast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import retrofit2.await
import java.io.IOException

class DetailViewModel @ViewModelInject constructor(
    private val pokeApi: PokeApiInterface,
    private val database: PokemonDatabase
) : ViewModel() {

    val isLoading = MutableLiveData<Boolean>()

    val liveDetailResponse = MutableLiveData<DetailResponse>()
    fun getDetailResponse(name: String) {
        isLoading.postValue(true)
        GlobalScope.launch(Dispatchers.IO) {
            try {
                liveDetailResponse.postValue(pokeApi.pokemonDetails(name).await())
                isLoading.postValue(false)

            } catch (e: IOException) {
                liveDetailResponse.postValue(null)
                isLoading.postValue(false)

            }

        }

    }


    val isSaving = MutableLiveData<Boolean>()

    @SuppressLint("DefaultLocale")
    fun saveAsFavorite(detailResponse: DetailResponse) {
        GlobalScope.launch(Dispatchers.IO) {
            isSaving.postValue(true)

            try {
                //Save all pokemon data
                database.pokemonDao().insertPokemon(
                    PokemonEntity(
                        detailResponse.id!!,
                        detailResponse.name!!,
                        detailResponse.baseExperience!!,
                        detailResponse.height!!,
                        detailResponse.weight!!
                    )
                )

                //Types
                val types = mutableListOf<TypeEntity>()
                for (type in detailResponse.types) {
                    types.add(
                        TypeEntity(
                            null,
                            type.type?.name!!,
                            detailResponse.id!!
                        )
                    )
                }
                database.typeDao().insertType(types)

                //Stats
                val stats = mutableListOf<StatEntity>()
                for (stat in detailResponse.stats) {
                    stats.add(
                        StatEntity(
                            null,
                            stat.stat?.name!!,
                            stat.baseStat!!,
                            detailResponse.id!!
                        )
                    )
                }
                database.statDao().insertStat(stats)

                //Abilities
                val abilities = mutableListOf<AbilityEntity>()
                for (ability in detailResponse.abilities) {
                    abilities.add(
                        AbilityEntity(
                            null,
                            ability.ability?.name!!,
                            detailResponse.id!!
                        )
                    )
                }
                database.abilityDao().insertAbility(abilities)

                //Moves
                val moves = mutableListOf<MoveEntity>()
                for (move in detailResponse.moves) {
                    moves.add(
                        MoveEntity(
                            null,
                            move.move?.name!!,
                            detailResponse.id!!
                        )
                    )
                }
                database.moveDao().insertMove(moves)

                delay(1000)
                isSaving.postValue(false)
                val message = "${detailResponse.name!!.capitalize()} " + BaseApplication.getAppContext().getString(R.string.detail_saved_message)
                noticeUser(message)


            } catch (e: IOException) {
                e.printStackTrace()
                delay(1000)
                isSaving.postValue(false)

            }

        }

    }

    @SuppressLint("DefaultLocale")
    fun removePokemonFromFavorites(pokemonId: Int, name: String) {
        GlobalScope.launch(Dispatchers.IO) {
            isSaving.postValue(true)

            try {
                //Delete local pokemon data
                database.apply {
                    typeDao().deleteType(pokemonId)
                    statDao().deleteStat(pokemonId)
                    abilityDao().deleteAbility(pokemonId)
                    moveDao().deleteMove(pokemonId)
                    pokemonDao().deletePokemon(pokemonId)
                }

                delay(1000)
                isSaving.postValue(false)
                val message = "${name.capitalize()} " + BaseApplication.getAppContext().getString(R.string.detail_removed_message)
                noticeUser(message)

            } catch (e: IOException) {
                e.printStackTrace()
                delay(1000)
                isSaving.postValue(false)

            }

        }

    }

    private fun noticeUser(message: String) {
        GlobalScope.launch(Dispatchers.Main) {
            toast(message)
        }
    }

    //Get user's favorite pokemon
    val liveFavoritePokemonList = MutableLiveData<List<PokemonEntity>>()
    fun getFavoritePokemon() {
        GlobalScope.launch(Dispatchers.IO) {
            try {
                liveFavoritePokemonList.postValue(database.pokemonDao().getAllPokemon())

            } catch (e: IOException) {
                liveFavoritePokemonList.postValue(null)

            }

        }

    }

    //Get local types
    fun getLiveFavoritePokemonTypes(id: Int): LiveData<List<TypeEntity>> {
        return database.typeDao().getAllPokemonTypes(id)
    }

    //Get local stats
    fun getLiveFavoritePokemonStats(id: Int): LiveData<List<StatEntity>> {
        return database.statDao().getAllPokemonStats(id)
    }

    //Get local abilities
    fun getLiveFavoritePokemonAbilities(id: Int): LiveData<List<AbilityEntity>> {
        return database.abilityDao().getAllPokemonAbilities(id)
    }

    //Get local moves
    fun getLiveFavoritePokemonMoves(id: Int): LiveData<List<MoveEntity>> {
        return database.moveDao().getAllPokemonMoves(id)
    }

}