package com.josueardondev.pokeapi.database.entitiy.ability

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface AbilityDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAbility(abilityEntities: List<AbilityEntity>): Array<Long>

    @Query("DELETE FROM AbilityEntity WHERE pokemon_id = :id")
    fun deleteAbility(id: Int)

    @Query("SELECT * FROM AbilityEntity WHERE pokemon_id = :id")
    fun getAllPokemonAbilities(id: Int): LiveData<List<AbilityEntity>>

}