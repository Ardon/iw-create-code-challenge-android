package com.josueardondev.pokeapi.database.entitiy.stat

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface StatDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertStat(statEntities: List<StatEntity>): Array<Long>

    @Query("DELETE FROM StatEntity WHERE pokemon_id = :id")
    fun deleteStat(id: Int)

    @Query("SELECT * FROM StatEntity WHERE pokemon_id = :id")
    fun getAllPokemonStats(id: Int): LiveData<List<StatEntity>>

}