package com.josueardondev.pokeapi.database.entitiy.ability

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class AbilityEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ability_id")
    var id: Int?,

    @ColumnInfo(name = "ability_name")
    var name: String,

    @ColumnInfo(name = "pokemon_id")
    var pokemonId: Int

)