package com.josueardondev.pokeapi.database.entitiy.type

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface TypeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertType(typeEntities: List<TypeEntity>): Array<Long>

    @Query("DELETE FROM TypeEntity WHERE pokemon_id = :id")
    fun deleteType(id: Int)

    @Query("SELECT * FROM TypeEntity WHERE pokemon_id = :id")
    fun getAllPokemonTypes(id: Int): LiveData<List<TypeEntity>>

}