package com.josueardondev.pokeapi.database.entitiy.move

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MoveEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "move_id")
    var id: Int?,

    @ColumnInfo(name = "move_name")
    var name: String,

    @ColumnInfo(name = "pokemon_id")
    var pokemonId: Int

)