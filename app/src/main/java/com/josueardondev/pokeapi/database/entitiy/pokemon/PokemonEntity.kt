package com.josueardondev.pokeapi.database.entitiy.pokemon

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class PokemonEntity(

    @PrimaryKey
    @ColumnInfo(name = "pokemon_id")
    var id: Int,

    @ColumnInfo(name = "pokemon_name")
    var name: String,

    @ColumnInfo(name = "pokemon_base_experience")
    var baseExperience: Int,

    @ColumnInfo(name = "pokemon_height")
    var height: Int,

    @ColumnInfo(name = "pokemon_weight")
    var weight: Int

) : Serializable