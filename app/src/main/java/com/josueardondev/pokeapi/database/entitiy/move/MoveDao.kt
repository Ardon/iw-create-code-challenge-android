package com.josueardondev.pokeapi.database.entitiy.move

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface MoveDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMove(moveEntities: List<MoveEntity>): Array<Long>

    @Query("DELETE FROM MoveEntity WHERE pokemon_id = :id")
    fun deleteMove(id: Int)

    @Query("SELECT * FROM MoveEntity WHERE pokemon_id = :id")
    fun getAllPokemonMoves(id: Int): LiveData<List<MoveEntity>>

}