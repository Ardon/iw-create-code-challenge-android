package com.josueardondev.pokeapi.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.josueardondev.pokeapi.database.entitiy.ability.AbilityDao
import com.josueardondev.pokeapi.database.entitiy.ability.AbilityEntity
import com.josueardondev.pokeapi.database.entitiy.move.MoveDao
import com.josueardondev.pokeapi.database.entitiy.move.MoveEntity
import com.josueardondev.pokeapi.database.entitiy.pokemon.PokemonDao
import com.josueardondev.pokeapi.database.entitiy.pokemon.PokemonEntity
import com.josueardondev.pokeapi.database.entitiy.stat.StatDao
import com.josueardondev.pokeapi.database.entitiy.stat.StatEntity
import com.josueardondev.pokeapi.database.entitiy.type.TypeDao
import com.josueardondev.pokeapi.database.entitiy.type.TypeEntity

@Database(
    entities = [PokemonEntity::class, AbilityEntity::class, MoveEntity::class, StatEntity::class, TypeEntity::class],
    version = 1
)
abstract class PokemonDatabase: RoomDatabase() {

    abstract fun pokemonDao(): PokemonDao
    abstract fun abilityDao(): AbilityDao
    abstract fun moveDao(): MoveDao
    abstract fun statDao(): StatDao
    abstract fun typeDao(): TypeDao

}