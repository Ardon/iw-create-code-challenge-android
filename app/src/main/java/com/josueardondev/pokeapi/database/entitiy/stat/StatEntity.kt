package com.josueardondev.pokeapi.database.entitiy.stat

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class StatEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "stat_id")
    var id: Int?,

    @ColumnInfo(name = "stat_name")
    var name: String,

    @ColumnInfo(name = "stat_base")
    var base: Int,

    @ColumnInfo(name = "pokemon_id")
    var pokemonId: Int

)