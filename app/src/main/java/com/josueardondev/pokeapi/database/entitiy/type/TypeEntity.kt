package com.josueardondev.pokeapi.database.entitiy.type

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class TypeEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "type_id")
    var id: Int?,

    @ColumnInfo(name = "type_name")
    var name: String,

    @ColumnInfo(name = "pokemon_id")
    var pokemonId: Int

)