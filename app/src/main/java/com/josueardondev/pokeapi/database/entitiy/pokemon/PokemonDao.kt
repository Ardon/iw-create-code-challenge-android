package com.josueardondev.pokeapi.database.entitiy.pokemon

import androidx.room.*

@Dao
interface PokemonDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPokemon(pokemonEntity: PokemonEntity): Long

    @Query("DELETE FROM PokemonEntity WHERE pokemon_id = :id")
    fun deletePokemon(id: Int)

    @Query("SELECT * FROM PokemonEntity")
    fun getAllPokemon(): List<PokemonEntity>

}