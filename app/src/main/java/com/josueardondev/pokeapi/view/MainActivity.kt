package com.josueardondev.pokeapi.view


import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.josueardondev.pokeapi.R
import com.josueardondev.pokeapi.adapter.NavigationAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // UI
        setupNavigationViewPager()
        setupBottomNavigationView()


    }

    // UI //

    private lateinit var mNavigationAdapter: NavigationAdapter
    private fun setupNavigationViewPager() {
        mNavigationAdapter = NavigationAdapter(supportFragmentManager)
        main_viewPager.apply {
            adapter = mNavigationAdapter

            addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(state: Int) {}
                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) { }

                override fun onPageSelected(position: Int) {
                    when (position) {
                        0 -> main_bottomNavView.setItemSelected(R.id.navPokemon, true)
                        else -> main_bottomNavView.setItemSelected(R.id.navFavorites, true)

                    }

                }

            })

        }

    }

    private fun setupBottomNavigationView() {
        main_bottomNavView.setItemSelected(R.id.navPokemon, true)
        main_bottomNavView.setOnItemSelectedListener { id ->
            when (id) {
                R.id.navPokemon -> main_viewPager.currentItem = 0
                else -> main_viewPager.currentItem = 1

            }

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                1 -> (supportFragmentManager.fragments[1] as FavoritesFragment).getLocalPokemon()
                else -> super.onActivityResult(requestCode, resultCode, data)

            }

        }

    }

}