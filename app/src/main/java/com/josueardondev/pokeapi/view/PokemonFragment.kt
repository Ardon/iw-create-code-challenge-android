package com.josueardondev.pokeapi.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.josueardondev.pokeapi.R
import com.josueardondev.pokeapi.adapter.PokemonAdapter
import com.josueardondev.pokeapi.api.model.pokemon.Pokemon
import com.josueardondev.pokeapi.util.*
import com.josueardondev.pokeapi.viewmodel.PokemonViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_pokemon.*

@AndroidEntryPoint
class PokemonFragment : Fragment() {

    //View model instance
    private val mViewModel: PokemonViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pokemon, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // UI
        setupRecyclerview()
        setupRefreshLayout()

        //Network
        observeLoading()
        observePokemonResponse()
        getPokemonResponse()

    }

    override fun onResume() {
        super.onResume()

        //Avoid progressbar bug
        pokemon_swipeRefreshLyt.apply {
            if (this.isRefreshing) {
                this.isRefreshing = false

            }

        }

    }


    // UI //

    private lateinit var mPokemonAdapter: PokemonAdapter
    private val mPokemonList = mutableListOf<Pokemon>()
    private fun setupRecyclerview() {
        mPokemonAdapter = PokemonAdapter(requireContext(), (activity as MainActivity), mPokemonList, null)
        pokemon_recyclerView.apply {
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = mPokemonAdapter

        }

    }

    private fun refreshRecyclerview(pokemonList: List<Pokemon>) {
        mPokemonList.clear()
        mPokemonList.addAll(pokemonList)
        mPokemonAdapter.notifyDataSetChanged()

    }

    private fun setupRefreshLayout() {
        pokemon_swipeRefreshLyt.setOnRefreshListener {
            //Get pokemon again
            getPokemonResponse()
            pokemon_swipeRefreshLyt.isRefreshing = false

        }

    }

    private fun showErrorLayout() {
        pokemon_recyclerView.setInvisible()
        pokemon_error_linearLyt.setVisible()
    }

    private fun hideErrorLayout() {
        pokemon_error_linearLyt.setGone()
        pokemon_recyclerView.setVisible()
    }

    private fun showProgressBar() {
        pokemon_error_linearLyt.setGone()
        pokemon_recyclerView.setGone()
        pokemon_progressBar.setVisible()

    }


    // Network //

    private fun observeLoading() {
        mViewModel.isLoading.observe(viewLifecycleOwner, Observer { loading ->
            if (loading) {
                showProgressBar()

            } else {
                pokemon_progressBar.setGone()

            }

        })

    }

    private fun observePokemonResponse() {
        mViewModel.livePokemonResponse.observe(viewLifecycleOwner, Observer { response ->
            if (response != null) {
                hideErrorLayout()
                if (response.results?.isNotEmpty()!!) {
                    refreshRecyclerview(response.results!!)

                }

            } else {
                showErrorLayout()

            }

        })

    }

    private fun getPokemonResponse() {
        if (isNetworkAvailable()) {
            mViewModel.getPokemonResponse()

        } else {
            showErrorLayout()

        }

    }

}