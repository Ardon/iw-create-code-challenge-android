package com.josueardondev.pokeapi.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.josueardondev.pokeapi.R
import com.josueardondev.pokeapi.adapter.PokemonAdapter
import com.josueardondev.pokeapi.database.entitiy.pokemon.PokemonEntity
import com.josueardondev.pokeapi.util.setGone
import com.josueardondev.pokeapi.util.setVisible
import com.josueardondev.pokeapi.viewmodel.FavoritesViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_favorites.*
import kotlinx.android.synthetic.main.layout_error.view.*

@AndroidEntryPoint
class FavoritesFragment : Fragment() {

    //View model instance
    private val mViewModel: FavoritesViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorites, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //UI
        setupErrorLayout()
        setupRecyclerview()

        //Database
        observeLocalPokemonList()
        getLocalPokemon()

    }


    // UI //

    private fun setupErrorLayout() {
        favorites_error_linearLyt.error_message_textView.apply {
            text = getString(R.string.favorites_error_message)
            setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_starmie, 0, 0)
        }
    }

    private lateinit var mPokemonAdapter: PokemonAdapter
    private val mLocalPokemonList = mutableListOf<PokemonEntity>()
    private fun setupRecyclerview() {
        mPokemonAdapter = PokemonAdapter(requireContext(), (activity as MainActivity),null, mLocalPokemonList)
        favorites_recyclerView.apply {
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = mPokemonAdapter

        }

    }

    private fun refreshRecyclerview(localPokemonList: List<PokemonEntity>) {
        mLocalPokemonList.clear()
        mLocalPokemonList.addAll(localPokemonList)
        mPokemonAdapter.notifyDataSetChanged()
    }

    private fun showErrorLayout() {
        favorites_recyclerView.setGone()
        favorites_error_linearLyt.setVisible()
    }

    private fun hideErrorLayout() {
        favorites_error_linearLyt.setGone()
        favorites_recyclerView.setVisible()
    }


    // Database //

    private fun observeLocalPokemonList() {
        mViewModel.liveLocalPokemonList.observe(viewLifecycleOwner, Observer {
            it?.let { list ->
                if (list.isNotEmpty()) {
                    hideErrorLayout()
                    refreshRecyclerview(list)

                } else {
                    showErrorLayout()

                }

            }
        })

    }

    fun getLocalPokemon() {
        mViewModel.getLocalPokemonList()

    }

}