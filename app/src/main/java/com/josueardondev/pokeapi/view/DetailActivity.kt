package com.josueardondev.pokeapi.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.TextView
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.josueardondev.pokeapi.R
import com.josueardondev.pokeapi.api.model.DetailResponse
import com.josueardondev.pokeapi.api.model.pokemon.Pokemon
import com.josueardondev.pokeapi.api.model.ability.AbilityResponse
import com.josueardondev.pokeapi.api.model.move.MoveResponse
import com.josueardondev.pokeapi.api.model.stat.StatResponse
import com.josueardondev.pokeapi.api.model.type.TypeResponse
import com.josueardondev.pokeapi.database.entitiy.ability.AbilityEntity
import com.josueardondev.pokeapi.database.entitiy.move.MoveEntity
import com.josueardondev.pokeapi.database.entitiy.pokemon.PokemonEntity
import com.josueardondev.pokeapi.database.entitiy.stat.StatEntity
import com.josueardondev.pokeapi.database.entitiy.type.TypeEntity
import com.josueardondev.pokeapi.util.*
import com.josueardondev.pokeapi.viewmodel.DetailViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.item_property.view.*

@AndroidEntryPoint
class DetailActivity : AppCompatActivity() {

    //View model instance
    private val mViewModel: DetailViewModel by viewModels()

    //Pokemon object
    private lateinit var mPokemon: Pokemon
    private lateinit var mFavoritePokemon: PokemonEntity
    private var mLoadLocally: Boolean = false

    //Permission request code
    private val WRITE_STORAGE_REQUEST_CODE = 123

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        mLoadLocally = intent.getBooleanExtra(Constants.EXTRA_LOAD_LOCAL, false)

        // UI
        setupDetailToolbar()

        if (mLoadLocally) {
            //Load data locally
            initActivityLocally()

        } else {
            //Load data from internet
            initActivityWithInternet()

        }

        //Database
        observeSaving()
        observeFavoritePokemonList()
        mViewModel.getFavoritePokemon()

    }

    override fun onBackPressed() {
        finishActivity()

    }

    private fun initActivityLocally() {
        //Retrieve pokemon from intent
        mFavoritePokemon = intent.getSerializableExtra(Constants.EXTRA_FAVORITE) as PokemonEntity

        //UI
        detail_swipeRefreshLyt.isEnabled = false
        detail_favorite_checkBox.isChecked = true
        setFavoritePokemonData()

        //Database
        observeFavoritePokemonTypes()
        observeFavoritePokemonStats()
        observeFavoritePokemonAbilities()
        observeFavoritePokemonMoves()

        //Enable pokemon share
        detail_toolBar.menu.getItem(0).setOnMenuItemClickListener {
            sharePokemon()
            return@setOnMenuItemClickListener true

        }

    }

    private fun initActivityWithInternet() {
        //Retrieve pokemon from intent
        mPokemon = intent.getSerializableExtra(Constants.EXTRA_POKEMON) as Pokemon

        //UI
        setupRefreshLayout()

        //Network
        observeLoading()
        observeDetailResponse()
        getDetailResponse()

    }


    // UI //

    private fun setupDetailToolbar() {
        detail_toolBar.apply {
            setNavigationOnClickListener {
                finishActivity()

            }

        }

    }

    private fun sharePokemon() {
        if (checkPermission(this@DetailActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            shareScreen(detail_container_scrollView, this@DetailActivity)

        } else {
            requestPermission(
                this@DetailActivity,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                WRITE_STORAGE_REQUEST_CODE
            )

        }

    }

    private fun setupRefreshLayout() {
        detail_swipeRefreshLyt.setOnRefreshListener {
            getDetailResponse()
            detail_swipeRefreshLyt.isRefreshing = false

        }

    }

    private fun showErrorLayout() {
        detail_container_scrollView.setGone()
        detail_error_linearLyt.setVisible()
    }

    private fun hideErrorLayout() {
        detail_error_linearLyt.setGone()
        detail_container_scrollView.setVisible()
    }

    private fun showProgressBar() {
        detail_error_linearLyt.setGone()
        detail_container_scrollView.setGone()
        detail_progressBar.setVisible()
    }

    @SuppressLint("SetTextI18n")
    private fun setPokemonData(detailResponse: DetailResponse) {
        //Name and render
        detail_name_textView.text = "#${detailResponse.id} " + mPokemon.name!!.capitalize()
        val imageUrl = Constants.IMAGES_URL + detailResponse.id + ".png"
        detail_render_imageView.setGlideImage(baseContext, imageUrl)

        //Pokemon properties
        detail_weight_textView.text =
            getString(R.string.detail_weight) + " ${detailResponse.weight}"
        detail_height_textView.text =
            getString(R.string.detail_height) + " ${detailResponse.height}"
        detail_baseXP_textView.text =
            getString(R.string.detail_base_exp) + " ${detailResponse.baseExperience}"

        //Display lists
        displayPokemonTypes(detailResponse.types, null)
        displayPokemonAbilities(detailResponse.abilities, null)
        displayPokemonMoves(detailResponse.moves, null)
        displayPokemonStats(detailResponse.stats, null)

        //Enable mark as favorite
        detail_favorite_checkBox.setOnClickListener {
            if (detail_favorite_checkBox.isChecked) {
                savePokemonAsFavorite(detailResponse)

            } else {
                removePokemonFromFavorites(detailResponse.id!!, mPokemon.name!!)

            }

        }

        //Enable pokemon share
        detail_toolBar.menu.getItem(0).apply {
            isEnabled = true
            setOnMenuItemClickListener {
                sharePokemon()
                return@setOnMenuItemClickListener true
            }

        }

    }

    @SuppressLint("SetTextI18n", "DefaultLocale")
    private fun setFavoritePokemonData() {
        //Name and render
        detail_name_textView.text = "#${mFavoritePokemon.id} " + mFavoritePokemon.name.capitalize()
        val imageUrl = Constants.IMAGES_URL + mFavoritePokemon.id + ".png"
        detail_render_imageView.setGlideImage(baseContext, imageUrl)

        //Pokemon properties
        detail_weight_textView.text =
            getString(R.string.detail_weight) + " ${mFavoritePokemon.weight}"
        detail_height_textView.text =
            getString(R.string.detail_height) + " ${mFavoritePokemon.height}"
        detail_baseXP_textView.text =
            getString(R.string.detail_base_exp) + " ${mFavoritePokemon.baseExperience}"

        //Remove pokemon as favorite
        detail_favorite_checkBox.setOnClickListener {
            removePokemonFromFavorites(mFavoritePokemon.id, mFavoritePokemon.name)
            finishActivity()

        }

    }

    //Type layout
    @SuppressLint("DefaultLocale")
    private fun displayPokemonTypes(types: List<TypeResponse>?, localTypes: List<TypeEntity>?) {
        if (!mLoadLocally) {
            for (typeResponse in types!!) {
                val view =
                    LayoutInflater.from(baseContext).inflate(R.layout.item_property, null, false)
                view.property_cardView.setCardBackgroundColor(getColor(R.color.colorPrimary))
                view.property_name_textView.text = typeResponse.type?.name?.capitalize()
                detail_types_flowLyt.addView(view)

            }

        } else {
            for (type in localTypes!!) {
                val view =
                    LayoutInflater.from(baseContext).inflate(R.layout.item_property, null, false)
                view.property_cardView.setCardBackgroundColor(getColor(R.color.colorPrimary))
                view.property_name_textView.text = type.name.capitalize()
                detail_types_flowLyt.addView(view)

            }

        }


    }

    //Stat layout
    @SuppressLint("SetTextI18n", "DefaultLocale")
    private fun displayPokemonStats(stats: List<StatResponse>?, localStats: List<StatEntity>?) {
        if (mLoadLocally) {
            for (stat in localStats!!) {
                val textView = TextView(baseContext).apply {
                    setTextAppearance(R.style.BodyText)
                    setPadding(0, 16.toDp(), 0, 0)
                    text = "${stat.name.capitalize().switchScript()}: ${stat.base}"
                }
                detail_stats_linearLyt.addView(textView)

            }

        } else {
            for (statResponse in stats!!) {
                val textView = TextView(baseContext).apply {
                    setTextAppearance(R.style.BodyText)
                    setPadding(0, 16.toDp(), 0, 0)
                    text = "${statResponse.stat?.name?.capitalize()?.switchScript()}: ${statResponse.baseStat}"
                }
                detail_stats_linearLyt.addView(textView)

            }

        }


    }

    //Ability layout
    @SuppressLint("DefaultLocale")
    private fun displayPokemonAbilities(
        abilities: List<AbilityResponse>?,
        localAbilities: List<AbilityEntity>?
    ) {
        //Create subviews for each ability item

        if (mLoadLocally) {
            for (ability in localAbilities!!) {
                val view =
                    LayoutInflater.from(baseContext).inflate(R.layout.item_property, null, false)
                view.property_cardView.setCardBackgroundColor(getColor(R.color.colorItemAbility))
                view.property_name_textView.text = ability.name.capitalize().switchScript()
                detail_abilities_flowLyt.addView(view)

            }

        } else {
            for (abilityResponse in abilities!!) {
                val view =
                    LayoutInflater.from(baseContext).inflate(R.layout.item_property, null, false)
                view.property_cardView.setCardBackgroundColor(getColor(R.color.colorItemAbility))
                view.property_name_textView.text =
                    abilityResponse.ability?.name?.capitalize()?.switchScript()
                detail_abilities_flowLyt.addView(view)

            }

        }

    }

    //Move layout
    @SuppressLint("DefaultLocale")
    private fun displayPokemonMoves(moves: List<MoveResponse>?, localMoves: List<MoveEntity>?) {
        //Create subviews for each move item

        if (mLoadLocally) {
            for (move in localMoves!!) {
                val view =
                    LayoutInflater.from(baseContext).inflate(R.layout.item_property, null, false)
                view.property_cardView.setCardBackgroundColor(getColor(R.color.colorItemMove))
                view.property_name_textView.text = move.name.capitalize().switchScript()
                detail_moves_flowLyt.addView(view)

            }

        } else {
            for (moveResponse in moves!!) {
                val view =
                    LayoutInflater.from(baseContext).inflate(R.layout.item_property, null, false)
                view.property_cardView.setCardBackgroundColor(getColor(R.color.colorItemMove))
                view.property_name_textView.text =
                    moveResponse.move?.name?.capitalize()?.switchScript()
                detail_moves_flowLyt.addView(view)

            }

        }

    }

    private fun removeAllTempViews() {
        detail_types_flowLyt.removeAllViews()
        detail_stats_linearLyt.removeAllViews()
        detail_abilities_flowLyt.removeAllViews()
        detail_moves_flowLyt.removeAllViews()

    }

    private fun finishActivity() {
        setResult(Activity.RESULT_OK)
        finish()
    }


    // Network //

    private fun observeLoading() {
        mViewModel.isLoading.observe(this, Observer { loading ->
            if (loading) {
                showProgressBar()

            } else {
                detail_progressBar.setGone()

            }

        })

    }

    private fun observeDetailResponse() {
        mViewModel.liveDetailResponse.observe(this, Observer { response ->
            if (response != null) {
                hideErrorLayout()
                removeAllTempViews()
                setPokemonData(response)

            } else {
                removeAllTempViews()
                showErrorLayout()
                //Disable sharing
                detail_toolBar.menu.getItem(0).isEnabled = false

            }

        })

    }

    private fun getDetailResponse() {
        mViewModel.getDetailResponse(mPokemon.name!!)

    }


    // Database //

    private fun observeSaving() {
        val dialog = showDialog(this, getString(R.string.detail_saving_message))
        mViewModel.isSaving.observe(this, Observer { saving ->
            if (saving) {
                dialog.show()

            } else {
                dialog.dismiss()

            }

        })

    }

    private fun savePokemonAsFavorite(detailResponse: DetailResponse) {
        mViewModel.saveAsFavorite(detailResponse)

    }

    private fun observeFavoritePokemonList() {
        mViewModel.liveFavoritePokemonList.observe(this, Observer {
            it?.let { list ->
                if (list.isNotEmpty()) {
                    //Check if current pokemon is in database to check as favorite
                    checkIfPokemonIsAlreadyFavorite(list)

                }

            }

        })

    }

    private fun observeFavoritePokemonTypes() {
        mViewModel.getLiveFavoritePokemonTypes(mFavoritePokemon.id).observe(this, Observer {
            it?.let { list ->
                if (list.isNotEmpty()) {
                    displayPokemonTypes(null, list)

                }

            }

        })

    }

    private fun observeFavoritePokemonStats() {
        mViewModel.getLiveFavoritePokemonStats(mFavoritePokemon.id).observe(this, Observer {
            it?.let { list ->
                if (list.isNotEmpty()) {
                    displayPokemonStats(null, list)

                }

            }

        })

    }

    private fun observeFavoritePokemonAbilities() {
        mViewModel.getLiveFavoritePokemonAbilities(mFavoritePokemon.id).observe(this, Observer {
            it?.let { list ->
                if (list.isNotEmpty()) {
                    displayPokemonAbilities(null, list)

                }

            }

        })

    }

    private fun observeFavoritePokemonMoves() {
        mViewModel.getLiveFavoritePokemonMoves(mFavoritePokemon.id).observe(this, Observer {
            it?.let { list ->
                if (list.isNotEmpty()) {
                    displayPokemonMoves(null, list)

                }

            }

        })

    }

    private fun checkIfPokemonIsAlreadyFavorite(favoriteList: List<PokemonEntity>) {
        val name = if (mLoadLocally) mFavoritePokemon.name else mPokemon.name

        for (i in favoriteList) {
            if (i.name == name) {
                detail_favorite_checkBox.isChecked = true

            }

        }

    }

    private fun removePokemonFromFavorites(pokemonId: Int, name: String) {
        mViewModel.removePokemonFromFavorites(pokemonId, name)
    }


    // Activity overrides //

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == WRITE_STORAGE_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                shareScreen(detail_container_scrollView, this)

            } else {
                //Notice user
                toast("Se denegó el permiso.")

            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        }

    }

}